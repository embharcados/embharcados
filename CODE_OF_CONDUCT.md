Nossos Valores
==========

Como pessoas participantes, colaboradoras e líderes, nós nos
comprometemos a fazer com que a participação em nossa comunidade seja
uma experiência segura e livre de assédio para todas as pessoas,
independentemente de idade, corpo, deficiência aparente ou
não aparente, etnia, características sexuais, identidade ou expressão
de gênero, nível de experiência, educação, situação sócio-econômica,
nacionalidade, aparência pessoal, raça, religião ou identidade e
orientação sexuais.

Comprometemo-nos a agir e interagir de maneiras que contribuam para
uma comunidade aberta, acolhedora, diversificada, inclusiva e
saudável.

Escopo
======

Este Código de Conduta se aplica dentro de todos os espaços da
comunidade e também se aplica quando uma pessoa estiver representando
oficialmente a comunidade em espaços públicos. Exemplos de representação
da nossa comunidade incluem usar um endereço de e-mail oficial, postar
em contas oficiais de mídias sociais ou atuar como uma pessoa indicada
como representante em um evento online ou offline.

Comportamentos Esperados
========================

**Seja acolhedor.** Nós nos esforçamos para ser uma comunidade que
recebe e apóia pessoas de todas as origens e identidades. Isso inclui,
mas não está limitado a membros de qualquer raça, etnia, cultura,
nacionalidade, cor, status de imigração, classe social e econômica,
nível educacional, sexo, orientação sexual, identidade e expressão de
gênero, idade, tamanho, situação familiar, crença política, religião e
capacidade mental e física.

**Seja atencioso.** Seu trabalho será usado por outras pessoas e você,
por sua vez, dependerá do trabalho de outras pessoas. Qualquer decisão
que você tomar afetará usuários e colegas, e você deve levar essas
consequências em consideração ao tomar decisões.

**Seja respeitoso.** Nós não concordaremos o tempo todo, mas a
discordância não é motivo para mau comportamento e falta de
educação. Todos podemos sentir alguma frustração de vez em quando, mas
não podemos permitir que essa frustração se transforme em ataques
pessoais. É importante lembrar que uma comunidade onde as pessoas se
sentem desconfortáveis ​​ou ameaçadas não é produtiva. Os membros desta
comunidade devem ser respeitosos ao lidar com outros membros, bem como
com pessoas de outras comunidades.

**Tenha cuidado com as palavras que você escolher.** Somos uma
comunidade de profissionais e temos uma conduta profissional.  Seja
gentil com os outros. Não insulte ou rebaixe os outros participantes.

Comportamentos Inaceitáveis
===========================

O assédio (público ou privado) e outros comportamentos de exclusão não
são aceitáveis. Isso inclui, mas não está limitado a:

-   Insultos pessoais, especialmente aqueles que usam termos racistas ou sexistas.
-   Atenção sexual indesejada.
-   Ameaças ou insultos dirigidos contra outra pessoa.
-   Piadas discriminatórias.
-   Perseguição.
-   Fotografar ou filmar sem o consentimento da outra pessoa.
-   Publicação de material sexualmente explícito ou violento.
-   Postar (ou ameaçar postar) informações de identificação pessoal de outras
	pessoas (*"doxing"*).
-   Defender ou encorajar qualquer um dos comportamentos acima.

**Se alguém pedir para você parar, pare.**

Aplicação
=========

A liderança da comunidade é responsável por esclarecer e aplicar nossos
padrões de comportamento aceitáveis e tomará ações corretivas
apropriadas e justas em resposta a qualquer comportamento que considerar
impróprio, ameaçador, ofensivo ou problemático.

A liderança da comunidade tem o direito e a responsabilidade de remover,
editar ou rejeitar comentários, commits, códigos, edições na wiki, erros
e outras contribuições que não estão alinhadas com este Código de
Conduta e irá comunicar as razões por trás das decisões da moderação
quando for apropriado.

Ocorrências de comportamentos abusivos, de assédio ou que sejam
inaceitáveis por qualquer outro motivo poderão ser reportadas para a
liderança da comunidade.

Todas as reclamações serão revisadas e investigadas imediatamente e de
maneira justa.

A liderança da comunidade tem a obrigação de respeitar a privacidade e a
segurança de quem reportar qualquer incidente.

Diretrizes de Aplicação
-----------------------

A liderança da comunidade seguirá as seguintes diretrizes para
determinar as consequências de qualquer ação que considerar violadora
deste Código de Conduta:

### 1. Ação Corretiva

**Causa:** Uso de linguagem imprópria ou outro comportamento
considerado anti-profissional ou repudiado pela comunidade.

**Consequência:** Aviso escrito e privado da liderança da
comunidade, esclarecendo a natureza da violação e com a explicação do
motivo pelo qual o comportamento era impróprio. Um pedido de desculpas
público poderá ser solicitado.

### 2. Advertência

**Causa:** Violação por meio de um incidente único ou atitudes
repetidas.

**Consequência:** Advertência com consequências para comportamento
repetido. Não poderá haver interações com as pessoas envolvidas,
incluindo interações não solicitadas com as pessoas que estiverem
aplicando o Código de Conduta, por um período determinado. Isto inclui
evitar interações em espaços da comunidade, bem como canais externos
como as mídias sociais. A violação destes termos pode levar a um
afastamento temporário ou permanente.

### 3. Afastamento Temporário

**Causa:** Violação grave dos padrões da comunidade, incluindo a
persistência do comportamento impróprio.

**Consequência:** Afastamento temporário de qualquer tipo de interação
ou comunicação pública com a comunidade por um determinado período.
Estarão proibidas as interações públicas ou privadas com as pessoas
envolvidas, incluindo interações não solicitadas com as pessoas que
estiverem aplicando o Código de Conduta. A violação destes termos pode
resultar em um afastamento permanente.

### 4. Afastamento Permanente

**Causa:** Demonstrar um padrão na violação das normas da
comunidade, incluindo a persistência do comportamento impróprio, assédio
a uma pessoa ou agressão ou depreciação a classes de pessoas.

**Consequência:** Afastamento permanente de qualquer tipo de interação
pública dentro da comunidade.

Atribuições
===========

Este código de conduta foi inspirado no [Código de Conduta do Django](https://www.djangoproject.com/conduct/)
e no [Código de Conduta de Colaboração](https://www.contributor-covenant.org/pt-br/version/2/0/code_of_conduct/).
